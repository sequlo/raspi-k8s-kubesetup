# raspi-k8s-kubesetup
Shell script for setting up a Raspberry PI Kubernetes cluster featuring:

* Each node:
    * Hostnames configured - `k8sm` for master node, `k8sw[0..n-1]` for `n` worker nodes, see https://bitbucket.org/sequlo/raspi-k8s-nodesetup for more details
* Master node:
    * /etc/hosts configured - So you can access the workers from the master node by hostname
    * SSH configured - A SSH Key is generated and installed on all the worker nodes for easy access from the master node
    * PSSH configured - For runnning ssh in parallel, an alias is installed so you can ssh all workers in parallel, try `pssh-workers` ;)
    * Kubernetes cluster created - Now we're talking
    * Kubectl configured - Default tool for working with your Kubernetes cluster
    * Flannel installed - A simple cluster networking implementation (https://github.com/coreos/flannel)
    * Helm installed - Version 3 so no Tiller required anymore :D!
* Worker nodes:
    * Workers added to Kubernetes cluster created on the master
* Kubernetes cluster:
    * MetalLB installed - I don't know about you, but I like to loadbalance my shit even on bare metal clusters (https://metallb.universe.tf)
    * Traefik - Running out of IP addresses creating LoadBalancer services all over the place... use an Ingress Controller (https://traefik.io)
    * Kuberetes Dashboard installed - For all the Windows users out there ;)

# Prerequisites
A Raspberry Pi flashed with Raspbian (this stuff is tested against Stretch Lite). Also it might be wise to place an empty file named `ssh` in `/boot` in order to gain SSH access to your Pi for headless setup (remember default user `pi`, password `raspberry`).

# How to Run
Login on the PI you wish to be the master node as user `pi` and execute the following steps.

## Step 1. Configure kubesetup
```bash
sudo curl -o /etc/kubesetup.conf https://bitbucket.org/sequlo/raspi-k8s-kubesetup/raw/HEAD/example.kubesetup.conf
sudo vi /etc/kubesetup.conf
``` 

## Step 2. Run kubesetup
```bash
sudo curl -o /usr/local/sbin/kubesetup https://bitbucket.org/sequlo/raspi-k8s-kubesetup/raw/HEAD/kubesetup
sudo kubesetup
```

# P.S.
If everything went well there should be a kubernetes and traefik dashboard available at a subdomain of the domain you specified in step 1. Suppose your domain is `example.com`, the dashboards will be accessible via:

* `kubernetes.example.com` - Just hit 'Skip' when the login procedure is prompted
* `traefik.example.com`

Setup your DNS so that `*.example.com` is routed to the IP of the LoadBalancer where traefik is beeing hosted. You can figure this out via `kubectl get svc -n kube-system`. If you did not specify a public domain then you could edit for example `/etc/hosts` for this purpose.
